var path = require('path');
var baseConfig = require('./webpack-base.config');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

baseConfig.output = {
    path: path.resolve('dist/js/'),
    filename: "bundle.js"
};

baseConfig.plugins.push(
    new HtmlWebpackPlugin({
        title: 'My App',
        filename: path.resolve('dist/index.html')
    }),
    new UglifyJsPlugin({
        uglifyOptions: {
            sourceMap: true,
            warnings: false,
            mangle: true,
            compress: {
                drop_console: false
            }
        }
    })
);

module.exports = baseConfig;