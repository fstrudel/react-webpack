var path = require('path');

module.exports = {
	context: path.resolve('js'),
    entry: ["./utils", "./app"],
    output: {},
	module: {
		rules: [
			{
				test: /\.(es6|js)$/,
				exclude: /node_modules/,
				loader: "babel-loader"
			}
		]
	},
	plugins: [],
	resolve: {
		extensions: ['.js', '.es6']
	}
}