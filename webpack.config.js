var baseConfig = require('./webpack-base.config');
var path = require('path');

baseConfig.output = {
	path: path.resolve('build/js/'),
	publicPath: '/public/assets/js/',
	filename: "bundle.js"
};

baseConfig.devServer = {
	contentBase: 'public'
};

module.exports = baseConfig;